package xyz.hellothomas.jedi.consumer.domain;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author
 */
@Getter
@Setter
public class ExecutorInstance {
    /**
     * 自增Id
     */
    private Long id;

    /**
     * namespaceName
     */
    private String namespaceName;

    /**
     * appId
     */
    private String appId;

    /**
     * executorName
     */
    private String executorName;

    /**
     * instance ip
     */
    private String ip;

    /**
     * 创建时间
     */
    private LocalDateTime dataChangeCreatedTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime dataChangeLastModifiedTime;
}